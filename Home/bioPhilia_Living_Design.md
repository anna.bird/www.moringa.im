+++
title= The Art Of Nature ~ BioPhilia
date= 2018-08-11
type= TradeAction
agent.type= 
+++

# Bio*Philia*~ WoMan is the object of Art
**Design Through Spirit, æsthetics of a natural law.**

## *Values and ethical relations to the natural world, reorientation of design through spirit.*
**Our evolutionary past, is a past in which we evolved with the rest of the biosphere, not separate from it or exempt from its laws.**

### The value of historical continuity whose traditional values serve and enrich the present and the future.
**Cultural Repair- Recognize the limits of cultural representation as a force for change by bring emotional truth to established principals. To initiate a new approach to current conditions.**

**Archaic Revival, design through spirit. Bringing cultural models that worked in the past, projecting them into a future possibility, living the creative process reorientated in a universe of possibility.**

**Let the gates give way between ourselves and Mother Earth. Ready for change, movable and modular. Adaptive systems for positive human adaptation and development; legacies of biological and cultural evolution must be considered and enjoined to promote resilience.**

**Present without resistance, free to turn and question, What do I want from here?.**

***A responce to the challenge of Our Life;* reForming a Relationship With Mother Earth, An Act Of Surrender.**

#### *The Only Source Of Knowledge Is Experience*
**To understand the importance of a healthy 'microbiome' in the body and the soil by awakening the transformative capabilities and natural healing potential within. Experience symbiotic healing; The healing process of a curing rather than the cure itself, a vitality greater than ones own.**

**The essential laws of all art, æsthetic science, cultivating a capacity to use inner resources.**

#### All Nature Is Watching This Drama
understand how the world works - love - dream and intention to connect

Radicaly break from the momentum of rationalism.

**Ones dream and the dream of the planet As One.**

# *'Vivarium ~ place of life'*
**Inventing a new universe to live in, a universe of posibility.**

## *Things change when you care enough to grab whatever you love, and give it eVeryThing.*

**A creative and interactive safe space for WoMen to explore, learn, reflect; on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces.**

### *The relationship between human and environment is highlighted, not human and 'things'.
**Naturaly tap into the abundant emotions of joy, grace, awe, wholeness, passion and compassion that we experience from beauty in nature.**

#### Experiences Reveil Creative Insight
**A Self-creative Exploration to insight deep and meaningful change by working with the individuals natural enthusiasm.**

**One easily reaches the heart and soul through the senses. 'Cosmic laughter' comes from, the suprise and delight of seeing the obvious, of which Mother Earth (the symbiotic real) is the source.**

#### Vivarium Built and Vivarium Online
**Knowledge by invention, the posibility for a harmony of the Built and Cultivated 'Vivarium' and Curated 'Online Salon Style Experiences', a new universe of posibilities.**

#### *Vivarium Online ~ Salon Style Experiences*
***Strengthening Relationships and commUnity by revitalizing the Art of Conversation.***

**To awaken posibility and creative insight, refine the taste and increase the knowledge of the participants through guided conversation organized around cultures and ideas reflective of the era in which we live.**

#### Designed To Be Fun, Creative, Interactive and Resourceful.
**An Artistic Mixed Media Curated Experience; combining quality live audio with digital and live visual aids that respect bandwidth.**

----

#### Set the context and let life unfold.
**Through educated experience, the body learns as a system how to improve upon its own movement patterns and personal habits as a way of altering potential lived experience.**

**Habits are built up over time and programmed into the nervous system.**

**Intuative intelligence; An open-minded awareness allows for an alteration of reactions to stimulus over time thereby developing new sensory experiences.**

**Knowledge of the 'evolutionary experience' and theory are shared here.**

**An effort to reach my own potential and help others do so in the process.**

#### *The way out is personal responsibiltity, the way out is in, soil to soul.*
**'Community' shall come to honor all of life sooner or later. The choices are; when that shall happen, and the quality of experience that one shall have as the community learns.** Create a Community based on the sum total of our personal acts of refermation.

# Ancient Technology For A Modern World
**Bio*Philia* By Design - Humans have an inherited need to _connect to 'nature' and other biotic forms due to our Evolutionary Dependence On It For Survival And Personal Fulfillment.**

**Mother Earth is conspiring with the 'game' asking for you to participate.**

## Connection with things that make living possible.
**Being attracted to all that is alive and vital, the human tendency to relate with life and natural processes - Is The Expression Of A Biological Need.**

**BioPhilia Living Design relates to Earth, Gaia as 'The Live And Dynamic Evolutionary Partner' in the shaping of the set, setting and the process.**

#### To find a safe and identifiable niche in the 'family', and the community and to position ones self to thrvive.**

**A creative and inventive, ecological design aproach to problem solving by constructing and developing naturally symbiotic, mutually beneficial and therapeutic systems to solve problems.**

### Ecological Enterprise
**A system that operates to deliver positve ecological impact and by that profit, produce a surplus.**

**In the long run, economy and environment are the same, if its un-environmental its un-economical, that is the Rule of 'Nature', Sustainability.**

#### Ecological Justice is Social Justice.
**Being Responsible Stewards of the Earth means taking to heart the current planetary healing crisis.**

**Designing and creating symbiotic living environments that support, restore and regenerate both the human body and the environment have positive effects on the health of a population as a whole, not just the individual within the living design.**

#### One has to learn what one needs to do.

**Each principle starts as a rough idea. Simple ideas that clearly communicate a specific course of action. Then, over time is refined and restated, before eventually becoming established as a core part of the decision making process.**

**A process of learning the ‘means-whereby’ in contrast to ‘end-gaining’.**

#### *Human Power is Eco Power.*

**The power to make better choices.**

#### EnvironMental Health Is EcoLogical Literacy.
**Literally!**

**Living as a symbol of exquisitness by choosing to relate to 'Mother Earth' as 'The Source' from which to conduct 'Ones Life' is 'Enlightenment'; the power by which humans understand the universe and improve their own condition.**

#### EnvironMental Health - Connecting soil to soul.
**Creating a harmonic habitat for the individual as a _biological organism in the _built environment within the _biophysical environment.**

**A ecologically realistic and productive environment where the human biofield is supported to function at its optimal potential, connected to 'nature'; things that make living possible.**

**Creating live and dynamic, open independant spaces that feel as they have _existed long before your arrival and will _remain long after your depart.**

**Humans draw emotional strength from the feeling of stability.**

**Learning to _catalogue experiences, the sources in our natural environment, of _positive emotional states that humans experience, then _recreate them in our built and cultivated environment to _enhance our emotional, mental, physical and biological health.**

**Using the symbiotic relationship with 'Earth' as a path of discovery into the unique characteristics of _intentional independant habitats, that not only can influence a spontanious therapeutic act but also can be used by the designer to open; they are dynamic, adaptable and can be experienced; offering a safe space and the possibility for trying out inventive solutions and transformation.**

#### Heal the soil heal the soul - becoming apart of your living design
**EnvironMental Health Is Real. An individual's unique expression plays an integral and constructive part in setting a direction. Healing envrionments allow one to draw emotional support from their settings through the Symbiotic Energy, the living energy of the space. One then feels _free to move around and interact unselfconsciously, to combine ones life with the life of the living systems and elements within the built space. Achieved by bringing in a number of high quality visual and intuitive interactions among the systems and elements of a space and its users. A living design built upon our inherent intuitive, 'biophilic' response to built forms, natural settings and systems, designed to generate healthy interactions by encouraging their spontaneous occurrence.**

**Psychobiology, where mind meets matter. The interaction between biological systems and behaviour.**

**Bio*Philia* Creative Living Design can host an integration of elements from 'Expressive Therapies' too 'Moral Therapy' and beyond; Creative Arts, dance, imaginative play, drama therapy, narrative, ... eco-psychology, somatics, nature therapy, geopoetics, adventure therapy, shamnanism, and body-mind practices. To name but a few, enter your creativity ...**

**Art and aesthetic experiences 'Cultivate The Senses', thereby making one more sensitive in social interactions. New ways of thinking, seeing and relating to the Natural World can be expanded, experienced. Causing a total shift of posture, preceptions, beliefs, and thought processes. Transforming ones relationship with the Natural World.**

**Encorporating past cultural ideals into this philosophical framework can contribute to the creation of new, future traditions and rituals. By emphasizing and contributing to 'cultural production' the concept can move beyond its own parameters contributing not only to the lives of its cultivators, curators and participants but also to the rest of society, ecologicaly.**

#### Social-ecological systems play a invaluable role in recovery and resilience by supporting the wellbeing that heals.

**Small and slow solutions. 'Growing' Cultivating a culture requires a good storyteller, with creative instruction and guidance. Changing a culture requires a discerning curator, a persuasive editor. There is no magic bullet, no single solution. No government or corportion can solve this. It is going to take the collective action of unique individuals, create your own 'road show', create your own luck.**

**Understand and develop important connections between the 'communicative structures' of, a physical built space, a community shaped through culture and ideas, and continuous forms of social contact grounded in a spirit of egalitarianism. Round table discussion can be critical to the progression of human sociability, discursive action, and participatory ecology.**

# The Potential Of Nature ~ Personal Empowerment
**Life is not an expectation to live up to, but a possibility to live into.**

**Refinement of ones character and capabilities leads to more meaningful contributions.**

## It's easy to destroy something, the challenge of Life is to Create something meaningful.
**The defining challenge of our time; Find the potential for the Cultivation of a more Attuned and Responsive Awareness of our Ecological Interconnection.**

**Today’s young Generation are being handed the biggest challenge of all; saving biodiversity the living 'Quality of Life' on this planet.**

**The indiscriminate use of the earth's resources, at a rate faster than the earth can regenerate, leave the next generation with shrinking reserves.**

**Not addressing immediate needs is an insult to the young. We must Act on the environMental realities we face.**

### Change Starts With Discussion ~ Revival of the 'Salon Style Culture'
**A rendezvous with the world of art and letters. The Art Of Communication.**

**The way is achieved through art and aesthetic experience, for everyone can share equally in its enjoyment.**

**Connect to the Feminine Spirit, the natural feminine touch to create harmony and order. It was believed that the 'Salon Style Culture' made for a better, more peaceful society.**

**With an increasing attention and need for gender equality and emphasis on the Gentleness of Women, 'salons' can offer an ideal situation for Women to Take a Naturaly Authentic Leading Role.**

#### Engage In The Art Of Cross-Culture Conversation in pursuit of Knowledge and commUnity.
**Lessons learned from communication, mixed media and human interaction today, can be cultivated within the 'Vivarium' through carfuly curated 'Online Salon Style Experiences' that plant seeds for New forms of World Community through Culture and Ideas.**

**The ideal salon participant is a person who is uniquely interesting and can offer fresh ideas, are well communicated and can advance the conversation.**

**Possessing an innate love of learning, a reflective intelligence with firmly held principles and demonstrates a genuine sensitivity and thoughtfulness towards others.**

**To be a good listener is as important as being a good orator, allowing more people to reach their own potential through engaged conversation.**

**Of course, many individuals who entere are not committed to these selfless ideals, as is often the case in any form of human association. But because the requirement to adhere to a principle of sincerity to participate in this community, the space itself can maintaine a natural authenticity.**

## *Curated to attract those individuals who are genuine in their effort to reach their own potential and help others do so in the process.*
**The exercise of human virtue, etiquette, sensitivity, and kindness to others are the paramount rules of online 'salon style' interactions.**

#### Cultivating Spheres Of Influence Through An 'accomplishment program'.
**With a dedicated core membership and Curated Salon Style Experiences that are open to new participants, contributors and guests.**

#### *Living in the relm of possibility.*
**Ideas and works in various subjects from ecology, philosophy, culture and living designs to literature, art, dance, music and morality can be among the many themes of the progam, whith others being personal. A space for ones creative expression, personal evolution and connection, larger than the achievment of specific goals. Designed to awaken creativity and highlight the obstructive nature of the limits we put on ourselves. Becoming aware of the fears, opinions and positions that we have about ourselves that stand in the way of simple fulfillment.**

#### *Asses your crops and pinpoint how you can thrive.*
**Spaces for discourse, create a culture of sociability in which the individual cultivates their own rational, moral, and aesthetic faculties. A community committed to humanistic, ecological ideas and intellectual enlightenment, reflective of the era in which we live.**

**More than pleasant social gatherings; Serious spaces for intellectual projects and advanced ambition, realistic ideas, outside the box creative minds. Online spaces to come for Creative Insight.**

**Every individual has a range of innate abilities and cognitive faculties that can be cultivated and should be developed for the entirety of one’s life. Salon Style Experiences can provide space for informal learning in which those who attend can improve their minds and acquire knowledge in a variety of Earthly subjects. Experiences are highly interactive and depend on the intellectual contributions of each of its members such that everyone has the opportunity to engage with the subject matter and further their own potential.**

#### Private And 'Intimatly Scaled' Salon Experiences
**Make it fesible to unveil and develope the best qualities of each participant.**

**By creating a space where everyone can understand the core belief system and character of other participants, and overcome the constraints of superficial, idle conversation. Hosts can have the opportunity (and responsibility) to acutely analyze the strengths and weaknesses of 'her' guests and reveal their intrinsic virtues while gracefully challenging their limitations through discourse.**

**Members can have the opportunity to share their personal projects and ideas and receive feedback from the group. Before a novel is published or a painting displayed, these works can be taken into the 'accomplishment program' for assessment or critique. This positive, critical response encourages the sharing of projects and promotes an open culture of creativity and collaboration.**

**Salon Experiences are enjoyable online social gatherings that focus on the happiness and pleasure of the members, sharing being a positive and self-affirming experience that encourages and facilitates self-development.**

**The abstract, philosophical conceptions of the Good are examined in conversations on values and ethical relations to the natural world, ... inquiry that informe the projects that take place in the 'salon' to create a better and more ecologicaly connected world 'through sustained discourse' and social interaction.**

#### A Rehearsal For Reality
**An open space in which participants can Test Creative Projects and Ideas in the process of Self-cultivation, unrestricted by the conventional boundaries of society.**

#### Curated Life - symbiotic evolution
**An intimate, theatrical and imaginative space for ecological discourse, a test bed for the values and desires to promote in society as a whole.**

#### Membership offers CommUnity for Intellectual Interactions
**Cultivated form a Rehearsal for Reality into a Reality. Creating a self-selecting commUnity that becomes an important fixture of daily life and a vital organ or ecoSystem for social ecological discourse. By exercising the virtues and compassion practiced in the space in all areas of ones life. The value system held in the 'Salon' become and are, part of the normative ecological framework of Ones Life.**

#### The Friendly, Lively, and Enjoyable Act of Sharing an Asthetic Experience.
**To Cultivate and Curate a foundational atmosphere of mutuality, collaboration and respect for others in a 'refined online environment' conducive to productive discourse and reciprocal exchange.**

**The public sphere presents communicative action in abstracted, sterile terms short on pleasurable context. Non-obligatory forms of association are not enjoyable as there is No Incentive To Participate.**

#### Aesthetic experiences make a design, an ultimately fulfilling and pleasurable environment evolves.
**Delighting guests and serving as a respite or wake up from their own stark reality. The magical, aestheticized space of the Vivarium Living Design sparkes one’s imagination and excites the emotions. Cultivating Intellectual Exploration; 'enhanced by full engagement of the senses', social ecological interactions are stimulated by the powerful force of Nature Herself, A Will To Self Delight, Your Biophilia.**

**Enhancing its popularity and contributing to its social and ecological impact both globaly and localy.**

#### Work is Love made Visible.
**The visual arts, music, poetry and literature are all important elements of a Mixed Media Online Salon Exprience.**

**By providing a shared audio visual experience with relatable knowledge, to naturaly incite communication and catalyze meaningful conversation amongst a diverse group.**

#### At a time when social and class divisions are deeply entrenched and widening the 'Online Salon Style Experience' can be revolutionary in promoting an ethos of inclusiveness.
**Rights to entrance are based on merit and an inquisitive spirit. Participants, self-selected of different genders, ecological cultures, socio-economic status, religious proclivities, and political orientations can come together as presumptive intellectual equals.**

**Self-educating experiences can elevate ones status from second-class citizens to venerated members of a new Ecological Evolution by forstering new independently created cultures and ideas at the heart of the chalenges of the time. Creating the possibility for a unique and creative, Global Ecological Cohesion.**

**Of course this is not always achieved and participants still come from relatively limited circles of the elite and privalaged, still a spirit of equality can be accomplished in certain fundamental ways. Ideological concern for inclusiveness in culture and ideas can contribute to a larger paradigm shift than from rigid class and status hierarchies.**

**Principles of equality, solidarity and community, by creating a healing space of ones own that becomes beneficial to society, 'social ecologicaly'. Sacred Symbiotic Healing Spaces; cultivated 'Vivarium' and curated 'Salon Style Experiences', plant the seeds of a new more progressive reality that are still in the process of becoming materialized.**

**Although the 'BioPhilia Living Design' or 'Vivarium' and 'Salon Style Experiences' combined can be a cultural, social and ecological force for change its ambitions are modest. Simply to bring people together for meaningful conversations to awaken posibility.**

**Salon Experiences do not dictate the ends of conversations, nor impose a political agenda or worldview, rather they embrace difference and contrasting ideologies. The power, interest, vitality, and socio-ecological efficacy depend upon the differences and diversity of the salon participants working together to define its ends.**

**The cultivated 'Vivarium' with curated 'Salon Experiences' can represent a 'microcosm of life'; the grief, beauty, intellectual thirst, banality, joy, the courage and weakness of humanity can all be represented, delivered in a concentrated form to spontaneously induce personal insight or revelation.**

**False ideas and prejudices would naturaly be examined. Predominant belief systems can be challenged, and confronted. With new perspectives and ways of seeing the world, society and ecology have no other choice but to evolve accordingly, symbioticly.**

**By taking place privately 'online' homes of women, they are naturaly and intentionaly designed to escape and can limit censure. 'Vivarium' Salons are cultivated and curated works, to create an affirmative and trustworthy whole environment in which participants are able to express themselves in ways that they dared not risk in other social contexts. The graceful touch of the host and moderation can allow for discourse to become impassioned and controversial while still remaining civil. The aesthetic dimensions of the cultivated 'Vivarium' salons both facilitate a positive ecological force and mask any unknown underlying potential, so that the salon can flourish relatively uncontested.**

**Awaken posibility, by incorporating aesthetics and sensitivity to inspire the natural capabilities to listen, learn, understand, and to empathize with others. The process of enhancing and refining such human capabilities generates forms of association that contribute to an individual flourishing. A space where mistakes are looked upon as fascinating oppertunities for growth and evolution, and guide us back to the higher purpose at hand.**

**Shared structures of social ecological design thinking, naturaly insight a deeper reflection on the purpose and significance of human existence.**

**In the hyper-accelerated world in which we live, the internet and social media “connect” people on an vast and expaesive scale, but can also isolate them in an endless chain of transient exchanges while craving vibrant 'face-to-face' conversations. People naturaly crave connection for a more in-depth development of ideas and belief systems.**

**Seeking deeper meaning and more profound connections with others, soil to soul.**

**There is a palpable sense of fatigue from a society immersed in a vacum of consumerism, social media posts and narcissistic presentations, driven by matirialism and consumption. With so much attention given to mistakes and criticism, the voice of the soul is literaly interrupted. Few exist that offer an alternative culture, or form of ecological community, a voice in society.**

**'Vivarium Online Salons' can be a way of addressing this modern dilemma. Offering an essential foundation for online communication that has been corroded in the process of technological advancement, (just like much of the built advancements) by creating communities around ideas in the real world where they matter most. Global think tanks for local impacts. By addresses internet exchanges veiled in an anonymity which often fuel hatred, ignorance, and misunderstanding, in a salon, one must argue cogently for and 'publicly' justify one’s ideas, (within a small self-selected group). An ideal salon experience facilitates sustained contact, with people from diverse backgrounds who think differently, leading to a greater possibility to overcome false biases and wrong assumptions. It is easier to feel compassion for, understand, and act responsibly towards a person when you interact on a regular basis, as you'll have a more developed knowledge of their personal history, lifeworld, and underlying motivations.**

**The 'Social Ecology' of a 'Salon Style Experience' naturaly offers Hosts and Guests a positive and vital opportunity to engage with a self-selected global ecological thinking community, through a subtle balance of sensitively structured interactions curated to foster self-development, empathy, and collective understanding, which is the basis for a more just, equitable, and secure Society and Ecology as a Whole.**

**In a world divided, at a moment when ecological discourse has taken a decidedly urgent turn, threatening social stability and international diplomacy, in a time where prejudice, racism, and intolerance are deeply entrenched, a renaissance of a 'salon style culture' offers a vehicle for reviving principles of participatory discourse, the social and ecological potential of which is limitless.**

#### Human-induced impacts on the “Earth System”
**Design Productive Creative Eco-Living spaces that are environmentally supportive and sustainable / duplicable. This system not only benefits our personal health and well-being but symbioticly benefits the environment.**

**Directly impacting many of the EnvironMental Realities we face today and leads to benefits that endure far beyond the immediate needs for basic necessities.**

**Addressing:**

- Climate Change
- Water Pollution
- Waste Disposal
- Land Management & Urban Sprawl
- Loss of Biodiversity
- Ocean Acidification
- Environment Related Personal Health Issues
- and so much more...

**Covering:**

- Medicinal, Functional Food Gardening design and plant selection
- Eco-Pro*Biotics* Water rehabilitation
- Bokashi Pro*Biotic* Soil rehabilitation composting
- Pro*Biotic* Medicinal food from milk fermentation (Kefir milk)
- Food Storage - Laco fermenting
- Designing for healthy body movment

**This system offers a radical approach to: Restorative Environments and CommUnity-based Ecological Restoration.**

**Focused on:**
- ***Personal empowerment, care and well-being.*** Becoming apart of the circle of life.
- Local Economy Global Community
- Care of Soil and Water a basic social responsibility, practicing the law of return.
- Care of Energy
- and more ...

#### *The work of the future*- ecological landscape design and management - Soil is carbon stored.
**Restoration and Reforestation - Biological Conservation - Regenerative Agriculture - Native Species Nursery ...**

**Biomimetics or biomimicry** - Catch and Store Energy - Water Energy. The imitation of the models, systems, and elements of nature for the purpose of solving complex human problems. The terms "biomimetics" and **"biomimicry" derive from Ancient Greek: bios- life and mimesis- to imitate.** Non-polluting, local, small-scale energy that do not diminish biodiversity, change the climate or reduce beauty. 

**Plant baised compostable plastics don't work. It't the single use convienience culture that needs to change not the materials. The way to reSolve any problem is through; protracted and thoughtful observation. Often we tackle a problem by dealing with a symptom of the problem, rather than the cause. To reSolve any Problem we need to Re Run (re-Solve) the system, experiment or experience, ... . Then through thoughful observation, understand where and when energies go from positive to negative outcomes and make changes to meet the needs of every part of the system.**

#### *CommUnity ~ common unity*
**Partnership societies** exist, and partnership societies thrive and regulate themselves through a **symbiotic relationship to plants, ecological health.**

**Nomadism** - A livelihood form that is ecologically adjusted at a particular level to the **utilization of marginal resources, thus makes use of resources that otherwise would be neglected.** The nomadic way of life always tried to (and had to) **maintain an equilibrium between the resources of the natural environment and the needs of the people.**

**Perma Culture, Forest Garden and Urban Renewal** - Integrates **ecology**; (the branch of biology which studies the interactions among organisms and their environment), landscape, organic gardening, architecture and **agroforestry**; (bringing trees into the city and agriculture system), in **creating a rich and sustainable way of living.**

>**culture** - Middle English (denoting a cultivated piece of land): the noun from French culture or directly from Latin cultura ‘growing, cultivation’; the verb from obsolete French culturer or medieval Latin culturare, both based on Latin colere ‘tend, cultivate’ (see cultivate). In late Middle English the sense was **‘cultivation of the soil’** and from this (early 16th century), arose ‘cultivation (of the mind, faculties, or manners’) _ Oxford English Dictionary: **“The cultivation of land, and derived senses.”**

## Host-Microbe Symbioses

#### Earth is a continuous flow of vital energy, molecules are the ever flowing story. Explore the healthy mutualistic relationship between the bacteria within our bodies and the bacteria within the Earth, Gaia.

**Nobel Prize recipient Joshua Lederberg coined the term microbiome to describe “the ecological community of commensal, symbiotic, and pathogenic microorganisms that literally share our body space.”**

**Symbiotic interactions are fundamental to life as we know it, and range across a spectrum from parasitism to mutualism.** Animals and Humans rely on microbes for nutrient acquisition, immune system regulation and protection, while microbial parasites challenge fitness and or health. The line between who is a mutualist and who is a parasite may depend on small variations in the environment.

**All higher organisms live in symbiotic association with microorganisms.** Animals and humans alike are complex ecosystems each consisting of the host and its associated microbiome. Ecosystems are the foundations of the biosphere and they determine the health of the entire Earth system. A dynamic ecosystem is a productive ecosystem.

**Humans are changing the environment at an unprecedented pace, there is increasing attention on the causes and consequences of the loss of symbiotic equilibrium.**

### Soil Is Life Created, Transformed.
#### Discover, Grow and Celebrate the macrobiotic ecology: its species, interactions, food webs, and communities of cells and microbes.
**Gaia is a Microbiome, Stop the Killing.** - Soil is fragile. We are literaly killing our microbiome with our toxic lifestyles, anti-biotics, chemicals and chemical ladend foods that alter this grand vast unknown microbial balance in and around us. Humans rely on microbes to perform many important functions that we cannot perform ourselves. Microbes digest food to generate nutrients for host cells, synthesize vitamins, metabolize drugs, detoxify carcinogens, stimulate renewal of cells in the gut lining and activate and support the immune system. The bacteria living in and on us are not invaders but are beneficial colonizers. Learn how to care for our microscopic colonizers so that they, in turn, can care for our health.

#### Learn to Design For Water
**Water- It is vital for all known forms of life, even though it provides no calories or organic nutrients.**

**It is a universal solvent, dissolving more substances then any other liquid.**

**Clean Water is a social responsibility. Water is the main constituent of Earth's hydrosphere and the fluids of most living organisms. Poluted water is toxifying the planet. Think of water as an important resource, reduce use before trying to save water to fulfill your needs. In Urban Settings: save it, store it, use it, reuse it, clean it, release it.**

**A healthy microbiome, which consists of an overwhelming majority of beneficial probiotic bacteria, is one of the most fundamental aspects of good health. When the 'body and soil', is supported and functioning optimally, an individual is able to react with resilience when faced with a stressful situation. When an individual is able to reduce stress on the 'body', the natural healing process of the 'body' can occur. Soil is the foundation of all life, the network that connects.**

## *Design A Lifestyle For Change*
A Lifestyle Worth Recording, a Legacy as unique as we each are.



