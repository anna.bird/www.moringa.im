+++
title = "Lifestyles For Change"
date = 2019-07-22
sort_by = "date"
render = true
+++

# The Ecology Of Woman As Art
***We Are Our Ancestors Wildest Dreams.***
***A Unique Opportunity For The Creation Of Something New And Beautiful.***

## *Design Through Spirit*
**It's time to step out from the story of our present situation, and write a New Future. A new story about creating healing spaces and connecting Soil to Soul, the Symbiotic Real.**

#### **Mission: Personal empowerment through ecological literacy, to awaken possibility.**

#### *Vision: Supporting Women Creating A Legacy With Mother Earth.*
**Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle.**

## *Anna ~ Private Consulting / Live Online 'Salon Style' Experiences / Public Speaking*
### **Valuing life, relationships, experiences, and soul-care.**
**Discovering and sharing solutions to everyday environMental impacts that we inflict on ourselves and our surroundings. Creating healthy living environments, turning the tables from an environMental withdraw to a environMental deposit for the future.**

**Born from a Culture of Soil Seekers, raised by a Pharmacist, Pilot, Goldsmith and Florist whom together share an active interest in energy efficient design, renovation and restoration. Thus my adventurous fascination of ecology and creative thinking was cultivated.**

**Certifications include; Herbalist, Mariner, PADI SCUBA Instructor, Underwater Naturalist Instructor, GIA Diamond Grader, Reiki Master, ... .**

**With a diverse spectrum of Design and Ecological Health related knowledge from 25+ years of Self-initiated Fieldwork Experience, designing, launching and running new ideas. Cultivating an ability to find and act upon opportunities to translate inventions or technologies into products and services that continues to this day.**

**Privately collaborating with individuals and small groups. Designing and hosting unique meaningful ecological experiences since 1994, Eco-Built Living spaces since 2004, public speaking since 2009.**

**Actively invest in and share my background, unique experiences, network and resources into What I do and Whom I work with. To inspire, nourish and support environMental Creativity toward developing the potential given by Mother Earth.**

#### The ecology is the economy
**If its un-environmental its un-economical, that is the Rule of 'Nature', Sustainability.**

#### *Connect and Co-create*
**The beginning of any change is discussion.**

# *Vivarium Online ~ Salon Style Experiences*
***Strengthening Relationships and commUnity by revitalizing the Art of Conversation.***

**A creative, interactive space for WoMen to explore, learn, reflect; on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces.**

## Live Hosted In Traditional 'Salon Style Culture'
**An effort to reach ones own potential and help others do so in the process.**

**To awaken possibility, refine the taste and increase the knowledge of the participants through guided conversation organized around cultures and ideas reflective of the era in which we live.**

### Designed To Be Fun, Creative, Interactive and Resourceful.
**An Artistic Mixed Media Curated Experience; combining quality live audio with digital and live visual aids that respect bandwidth.**

#### Experiences Reveal Creative Insight
**A Self-creative Exploration to insight deep and meaningful change by working with the individuals natural enthusiasm.**

#### Custom Salon Experiences Available.
**Valued As A Rehearsal For Reality; The core ideal, free thinking and the ability to question the surrounding world.**

**An atmosphere where discussions can flourish, a space in which participants can test creative projects and ideas in the process of self-cultivation.**

#### Leave A Legacy That Grows
***Values and ethical relations to the natural world, reorientation of design through spirit.***

#### *The way out is personal responsibility.*
**Community shall come to honor all of life sooner or later. The choices are; when that shall happen, and the quality of experience that one shall have as the community learns.**

**There is an ecological story that needs to be told and passed on, about how it is to Live, to show respect and understanging for the place One Calls Home, Symbiotically Mother Earth.**

#### CommUnity Support
**Come together in common Unity "Online Global CommUnity for Local Impact".**

#### Online Live ~ *Vivarium WoMens Salon*
*Accomplishment Program - Private Please Inquire*

**Curated to attract those individuals who are genuine in their effort to reach their own potential and help others do so in the process.**


### Explore ~ Connect

[**Public Profile & Reviews**](https://www.airbnb.com/users/show/3774308)

**[YouTube](https://www.youtube.com/channel/UCJsiffL5swNOi37qDOsk8ZQ/videos)**

**Instagram - @vivarium_place_of_life**

[**Chat with me directly**](https://matrix.to/#/!ibYXXCkubbZiWtkmhX:matrix.org?via=matrix.org)

#### [*Payments with Paypal*](https://www.paypal.me/vivarium)


