+++
title= Lacto Ferments
date= 2019-02-04
+++

# Making Food Based Probiotics At Home

### Fermentation is a centuries-old method of food preservation.

> “To ferment your own food is to lodge an eloquent protest—of the senses—against the homogenization of flavors and food experiences now rolling like a great, undifferentiated lawn across the globe. It is also a declaration of independence from a group of fraudsters that would much prefer we were all passive consumers of its commodities, rather than creators of unique products expressive of ourselves and the places where we live.” ~ MICHAEL POLLAN, from the Foreword (The Art of Fermentation)

### Processed food is starving our microbiota of genetic stimulation, live-culture foods are rich depositories of bacterial genes that are part of our human cultural legacy.

# Vegetable Fermentation

While other food preservation methods such as canning and drying seeks to eliminate bacteria and
other microorganisms lactic fermentation is a symbiotic relationship between humans and bacteria.

The actual microbial process is complex and involves the natural conversion of starches and sugars in food
into lactic acid through a succession of acid producing anaerobic bacteria. Species of Caliform
bacteria starts the fermentation and begins to produce lactic acid. As the acidity of the environment
increases species of Leuconostoc bacteria succeed and continue to produce acids. As the pH
continues to drop species of Lactobacillus succeed and proliferate. The lactic acid creates an
environment so acidic that putrefying bacteria cannot survive. In this way lactic acid works as a
natural preservative.

## The Process

The key component of lactic fermentation is the creation and maintenance of an anaerobic and acidic
environment in which lactobacillus can thrive. 

To ferment food in this fashion a few basic materials
are needed:

- Vegetables to ferment (any can be fermented, but some things may develop a mushy texture)

- Sea salt (non iodized) - inhibits many organisms, but is tolerable up to a point by lactobacilli.

- A ceramic crock pot or glass container, and some sort of plate or item to use as a weight

1. Chop vegetables - any way you like - choppers choice!

2. Either: sprinkled with sea salt. About 1 tablespoon salt per two pounds of cabbage is a good
rule of thumb. The salt draws water out of the vegetables, creating a brine. Massage the
cabbage by hand or mix thoroughly with a wooden spoon to break down the cell wall of the
cabbage and draw out water.

3. Or: create a brine by mixing sea salt and non-chlorinated water. 

**Brine Rito: 3 Tablespoons of Salt with 4 cups of water ** 

4. The vegetables are then put into a container and pushed down until the brine completely covers
the top of all the vegetables by at least two inches. The brine creates the anaerobic
environment that favors the growth of lactobacilli, and serves as a protection against the growth of
putrefying microorganisms.

5. Once the crock is filled and the brine is completely covering everything, a weight (follower) should be
placed on top to keep everything submerged under the brine. Without a weight (follower) and cover the
vegetables may float to the top where exposure to air makes them mold.

6. Leave the crock to ferment! The minimum temperature range for sauerkraut is 50-65 degrees,
optimal is 65-90 degrees and maximum is 90-112 degrees. After about three days the ferment will
taste acidic, and will increase in acidity overtime. The longer kraut is left to sit the more time it
will have to go through all its microbial transformations and produce the most probiotic
kraut. Vegetables under the brine will remain edible for up to eight months whereas they will
begin to get mushy. Cold storage may also help the ferment keep for longer.

## Tips For Beginners
- Colder and saltier = slower ferment. Warmer and less salt = faster ferment.

- Check & taste your ferment every day – this way you’ll be able to skim off any film that form and get a sense for the fermented flavors you like (flavors change daily)

- If you get a white film, it’s OK. Just skim off the film, remove any discolored/darkened vegetables, and wash your weight.

- Avoid fermenting in metal or plastic – these will dissolve/leach into the food.

- Avoid antibacterial soaps. For sterile jars and hands, use vinegar to rinse.

- All the “starter” you need is on the vegetables, you don’t need to buy anything special or keep a starter culture alive between ferments.

- Don’t Can: Canning diminishes the health benefits and is a lot of work compared to cold storage.

## Health and Nutrition

Eating live, fermented foods directly supplies the digestive tract with living cultures that helps build
and develop one's cultural ecology. Eating a variety of live fermented foods promotes diversity among
microbial cultures in ones body. The main byproduct of lacto-fermentation is lactic acid which
promotes the growth of healthy flora throughout the intestine. This flora inhibits the growth of diarrhea
related bacteria such as Shigella, Salmonella, and e.coli. Human intestines are lined with a mucosal
cell surface which acts as a protection against stomach acids, lactobacillus compete with potential
pathogens and protect the mucosal cell surface. They are essentially occupying space that
pathogens would be occupying. Eating fermented foods is recommended to prevent a variety of
gastrointestinal conditions including diarrhea, constipation, ulcers, and various other digestive
disorders.

Fermented foods are also highly nutritious. According to the United Nations Food and Agriculture
organization, fermentation improves bioavailability of minerals present in food. As they go through
their cycle of succession microbial cultures create B-vitamins including folic acid, niacin, thiamin, and
biotin. Lactobacilli creates omega 3 fatty acids, essential for cell membrane and immune system
function. Lacto-fermentation can remove toxins from food. Examples of this are tropic ferments of
cassava or taro, both which are toxic unless fermented. Ferments must be eaten raw to obtain the
full spectrum of nutritional benefits.

## “At the still point, there the dance is ... neither arrest nor movement ...” ~ T.S. Elliot

## Creating Cultures That Heal.

### Fermentation is a tangible way of interacting with the larger web of life, a way to cultivate and reclaim our relationship with Nature, Gaia.

#### Natural fermentation precedes human history, a natural phenomena that our Neolithic ancestors observed and learned how to cultivate. When we study fermentation, we are in fact studying the most intimate relationships between WoMan, microbe and Gaia, Earth.


