+++
title= Fermented Skin Care Workshop Overview 
date= 2019-03-30
+++

# Fermented Skin Care Secrets Revealed

## **Learn the way to produce your own high quality therapeutic skin care treatments.
**Fermented ingredients are like a supercharged turbo boost for your skincare routine. Naturally; Antiseptic, Anti-Bacterial, Anti-Fungal and Anti-Viral.

### *Nourishing and gentle the Whole Family even Mother Nature can enjoy.*
**Remarkably enough our Skin, the Water and all that come in contact with that, the Soil, can find natural healing benefits from probiotics with this one easy process.**

#### Self-care that goes far beyond just the self.
**Explore an alternative way to heal and support the body, that can heal and support our environment.**

**A probiotic rich super food for the microbiome of our bodies, soil to soul.**

**Engaging “Show & Tell” approach, this class will cover:**

- Overview of a probiotic approach to beauty and wellness.
- The Science behind fermented skin nutrition treatments.
- Introduction of a broad range of natural and seasonal ingredients.
- Live Demo Covering All Fermentation Steps and details to craft your own body, personal hygiene products and all-purpose cleaner at home.
- Insider Information:  tap into over 10 years of knowledge, discussion of multiple uses and application methods.

Online Experience - 60 minutes.
Q&A/Salon Style Open Talk - 15 minutes to close.

Please let me know upon booking what your level of interest is,  your intended use ... any questions you have that helps me prepare for the session better and save time so I can get right to sharing and be sure to cover what you want to learn about. We will do a Q&A at the end of the session.

