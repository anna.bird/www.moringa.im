---
title: Fermented Beauty Secrets
date: 2018-08-13
---

# *Luxurious Fermented Skin Care Secrets Revealed*

## *Anything will give up its secrets if you Love it enough.*

### Learn the way to produce your own high quality nourishing fermented skin care treatments for the whole family that can truly rival their manufactured counterparts.

#### Clean Beauty, Green Beauty  **Eco-Pro*Biotics* - Fermented fruit and peel enzymes are naturally; Anti-Bacterial, Anti-Fungal, Anti-Viral and a Natural Antiseptic.**

**This exact process presented here can also be used to make an all-purpose cleaner that can be used to wash everything from your bicycle to your laundry.**

## Delicously rich, gourmet food for the microbiome of your skin. Fermented ingredients are like a supercharged turbo boost for your skincare routine.

**A healthy skin microbiome, which consists of an overwhelming majority of beneficial probiotic bacteria, is one of the most fundamental aspects of good health.**

**Fermentation naturally produce AHAs, the all-important ingredient that helps slough away dead skin to reveal its natural gorgeous glow. AHA- Alpha hydroxy acids work by “loosening” the top layer of dead skin cells on your skin, revealing the healthy, newer skin underneath.**

**Lactic acid, the gentle sister of the AHAs and the ideal choice for those with sensitive skin. Lactic acid is known for making the skin extremely soft while also brightening those PIH (post-inflammatory hyperpigmentation) spots, improving skin tone, and increasing the collagen production in your skin. Like AHAs it also helps with acne, blackheads, and whiteheads.**

### Lactic acid bacteria (LAB) are one of the most well-studied bacterial groups known from ancient times. LAB are part of normal microflora of gastrointestinal and genitourinary tracts, hence they are used as components of probiotics.

- **Working by accelerating skin cell turnover (exfoliating) and act as a moisturizer and humectant, meaning it draws water into the skin, helping to plump-up the skin and give you a smoother, more youthful appearance.**

- **Lactic acid works to clear pores and control breakouts on face and body without stripping moisture levels. Gently removes dirt and oil without over-drying your skin, and helps protect skin from environmental aggressors.**

- **Lactic acid can be used to help prevent and reverse signs of aging and sun damage, improve uneven skin tone, acne and dull looking skin. Reported to be effective in treating uneven skin tone or pigment issues like melasma, sun spots and postinflammatory hyperpigmentation (the darkness that gets left behind after acne, a burn or injury to the skin).** 

**Fermented skincare products are naturaly teaming with antimicrobial goodness, these bacteria inhibit the growth of pathogens naturaly eliminating the need for preservatives.**

> Look deep into nature, and then you will understand everything better. ~ A. Einstine

## *There is a microbiome connection between our Skin and the Soil.* 

Biome- a large naturally occurring community of flora and fauna occupying a major habitat.

**Remarkably enough our Skin, the Water and all that come in contact with that ... the Soil, can find natural healing benefits from probiotics with this one easy process.**

**Yes, Nature is that simple, effective and synergistic.**

**Excerpt below from "The Art Of Fermentation" ~ Sandor Katz**

> # Bacteria: Our Ancestors and Coevolutionary Partners

> "Biologists use the term fermentation to describe anaerobic metabolism, the production of energy from nutrients without oxygen." 
> "Bacterial fermentation processes have been part of the context for all life. Fermentation plays such a broad and vital role in nutrient cycling that all beings coevolved with it, ourselves included. Through symbiosis and coevolution, bacteria fused into new forms, spawning all other life. “For the past (billion) years, members of the Bacteria superkingdom have functioned as a major selective force shaping eukaryotic evolution,” state molecular biologists Jian Xu and Jeffrey I. Gordon. “Coevolved symbiotic relationships between bacteria and multicellular organisms are a prominent feature of life on Earth.**”The importance of bacteria and our bacterial interactions cannot be overstated. We could not exist or function without our bacterial partners."**

# **Stop the killing Gaia is a Microbiome** 

## Create Cultures That Heal

## Be Pro*Biotic*; for life, and feel better! We are literaly killing our microbiome with chemicals and chemical ladend foods that alter this grand vast unknown microbial balance in and around us.

**Cleaning products made with chemicals pollute our bodies, the underground water, rivers and surrounding eco-systems. These conventional cleaning and body products contain harmful chemicals such as phosphate, nitrates, ammonia, chlorine ... etc. The accumulated effect of these chemicals released from every household causes significant damage to our Whole Health the Environment; Gaia, the blue ball we all call Home.**

**What are probiotics? Science defines- Probiotics are live micro-organisms that, "when administered in adequate amounts, confer a health benefit on the host."**

**Pro- ‘for, on behalf of’.**

**Biotic- ‘life’.**

**Why make Eco-Pro*Biotics*? *On Behalf Of Life.* A Gift of Life, given by cleaning your body and home with Eco-Pro*biotics*. More than skin deep, soul food for the microbiome of your Skin and the Soil, Gaia.**

#### **We Are One ~ Body And Soil Microbial Gardens** 

**We're more microbe than we are person. Our bodies contain 10 times more bacteria than human cells, meaning around 100 trillion microbes make up who we are. We are Wild in this way. The largest colony is located in the stomach and intestines. Our bacteria digest substances that help our immune systems defend against invaders. This exact same process happens in soil, and the food that comes from it. Protect your microbes with Probiotics. Doing so will keep your microbiome full of live, beneficial organisms, which will help keep digestion, immunity and overall health on track. Body and Soil, encourage the growth of healthy bacteria.**

**What all cultured 'foods' have in common is an abundance of lactic acid bacteria, yeasts, and moulds that produce enzymes which break down and alter the original foods. The microbes eat the sugars, digesting them and excrete metabolites. There are many targeted uses for these metabolites. Companies package them and sell them at a high cost. The majority of products on the market are powdered or freeze-dried, this process causes damage to the microbes, resulting in 90 to 95% die off. This means, even though colony counts are high, only 5 to 10% of the microbes are actually alive.** 

#### *Few create a product that can truly rival real, live ferments.*

**Eco-Pro*Biotic* is a simple process for encouraging natural fermentation by lactic acid-forming bacteria such as Lactobacillus species and naturally-occurring wild yeasts of fruit and plant food remnants and brown sugar. The fermentation process produces amino acids and peptides that help skin’s cellular renewal.**

**Fermentation is a kind of detoxification process, a transfer of anti-nutrients into nutrients, nutrient enhancement and the creation of micro-nutrients occurs. This process also increases the bioavailability, making it so your skin is able to absorbe the nutrients, for brighter and more hydrated skin. The process creates new amino acids, organic acids, and antioxidants that didn’t originally exist in the raw material, naturaly optimized to provide the most nutrition to your skin. The fermentation process transforms the natural substances into a lightweight essence that immediately seeps into skin and mildly exfoliates it with lactic acid. A product that can be applied as a moisturizer, serum or mask, enabling the skin to more readily accept and absorb these ingredients.**

# Strengthen The Microbiome Of Your Skin.

## **Pro*biotics* are live bacteria and yeasts that are healthy for you and your enviroment.**

### Empower your life by living in harmony, heal your body, home and the enviroment. Feel vitality and strength by cultivating a symbiotic relationship with Nature. With Eco-Pro*Biotics* your literally nourishing your Skin and feeding that same goodness into the Water your using.

### Eco-Pro*Biotics* can be part of a solution to some very big problems: **Loss of Biodiversity / Microbiome** and **Waste Disposal.**

## **Naturaly Impacting**

- Climate Change
- Water Pollution
- Waste Disposal
- Loss Of Biodiversity / Microbiome
- Land Management & Urban Sprawl
- Community Building

#### **Some Eco-Pro*Biotics* Benefits:**

**Community Building**, make Eco-Pro*Biotics* apart of your community, passivly getting others involved by collecting unutilized food remnants from your area. 
 
**Safe**- fermented products mimic cell functions, working with skin in a symbiotic fashion, adverse reactions are rare. Eco-Pro*Biotics* are suitable for sensitive skin. Lactic acid bacteria inhibit or kill undesirable bacteria in food and in the human body by a number of mechanisms including the production of lactic acid and antimicrobial peptides (bacteriocins). 

Eco-Pro*Biotics* are non-toxic to birds, fish, aquatic invertebrates, and honey bees.

“The presence of lactobacilli as a part of the normal vaginal flora is an important component of reproductive health.” ~ E. R. Boskey et al., “Origins of Vaginal Acidity: High D/L Lactate Ratio Is Consistent with Bacteria Being the Primary Source,” Human Reproduction 16(9):1809 (2001).

**Reduce Pollution**- Methane gas released from disposed garbage can trap 21 times more heat than CO2, worsening the global warming condition. The solution... fermentation. No heat or toxic gass is produced in the fermentation process. Any plant matter can be used in this process, we chose citrus and pineapple peel for there naturally powerful cleaning abilities and pleasing aroma.

**Additionally, LAB action reduces or eliminates pathogenic bacteria such as Salmonella species and Escherichia coli.**

**Multiple Usage**- Natural household cleaner, air purifier, detergent, car care, fabric softener, organic fertilizer, etc. Lactic acid well known for its descaling properties and is widely applied in household cleaning products. Also, lactic acid is used as a natural anti-bacterial agent in disinfecting products.

**Purify Underground Water**- Eco-Pro*Biotics* flow underground and help to purify ground water and bring more biodiversity to the enviroment they come in contact with.

**Purify Air**- Remove odors from toxic air released from smoking, car exhaust, chemical residues of household products, etc.

**Clearing Drains**- Eco-Pro*Biotics* can be used for clearing city drains after a storm, and works magic in septic systems. 

**Stable**- If the enzyme is full of its necessary nutrients, it can survive even at pH. value below 3.5. A safe stable stage to shoot for is about 4.5. 

**Every time you make Eco-Pro*biotics* and release them into the enviroment your bringing more biodiversity to the enviroment around you. Done consistantly in a active community this easy process has great potential for impactful change.**

#### *What you can do with it:*

**From a baby bath to washing your bicycle! Eco-Probiotics can do it all, and more.**

- Cleansing the body - naturaly pH balancing, conditioner and moisturizer for the most sensitive skin. Suporting the skin in its ability to function and restore itself properly, creating a healthy barrier.
- Lactic acid is well known for its descaling properties, Eco-Pro*Biotics* suports oral health for your teeth and gums.
- Wound cleaner - burns, cuts, abrasions, bed soars, diaper rash, diabetic wounds
- Cleansing the whole home & drainage systems
- Kill, repel and trap insects
- Fertilize plants and rehabilitate soil
- Purify aquatic systems
- Cleanse pets & animal cages or enclosures 
- Fruit & Vegetable wash

**Fermented essences with water-like consistency are the easiest to incorporate into your routine because they tend to work for most skin types. They’re versatile and can be used regardless of the season.**

**Introducing fermented ingredients into your routine is extremely easy.**

## **How To Make Your Own Eco Pro*Biotics**

### The 1:3:10 Ratio

1. Sugar: 1 part
2. Organic waste: 3 parts
3. Water: 10 parts

### **Step 1:**
Fill vessel with water leaving room for expansion, and food waste (60%  of the
volume).

### **Step 2:**
Put 1 part brown sugar in the container and stir.

### **Step 3:**
Put up to 3 parts food waste into the container. Sometimes I add more for a more
thick brew. This is just a guideline, experiment and play. The smaller you chop
the pieces the better your ferment.

### **Step 4:**
Close the container and let the mixture ferment for 3 months. In the first month
you can open and have a peek giving a stir if you like. If your not using a
airlock you must do this daily! Leave the cap a little lose or put a balloon over
to avoid an explosion! A cloth with elastic is not suitable for this type of
fermentation. Date your bin!

### **Step 5:**
Harvest Day!!! In a suitable size vessel with a suitable size strainer, separate
the solids form the liquids. Your filtered probiotic can be bottled and is ready
for use following the dilution ratios provided (see attached chart). The solids
can be used as a starter for your next batch (once only), or dried and used as
a powdered probiotic fertilizer. Add them to your garden compost or turn them directly
into the soil. Even dumping them down the toilet is perfectly fine. Of course
you will want to make sure that no large pieces remain. Often it is possible to
simply mush it up into a paste with your hands. I have been tempted to use this
as a face mask, but have yet to experiment... .

**Fermentation time & environment:** 3 months minimum, from the time you stop adding food waste. Store in a dark environment away from excessive heat.

**TIPS and Details:**

**Use brown sugar**, jaggery or some similar sugar, it is full of minerals and will leave you with a more rich product.

**Separate your kitchen waste**, think aroma and decomposition. Orange, Lemon, Pineapple, Apples, … Citrus are beautifully aromatic and make powerful cleaners.

**Use Airlocks for an anaerobic environment.** Keeping your ferment clean and free of insects. This is important for a nice functional product and for safety, containers may burst if the gases are not released.

If adding the organic matter slowly over a period of time, first fill the container to halfway with water and dissolve the sugars inside. Once your ratio of organic matter is full, top up the rest of the water and close the lid, starting the 3 month ferment.

Different organics have different benefits, aromas and textures. You can separate citrus fruits like Oranges, Lemons and Lime into a dedicated bin, as well as Pineapple, which makes for a great cleaning solution, and other aromatics like Ginger, Lemongrass, and Pandan can be added for a beautiful whole body washing product.

The list goes on! It’s a great idea to have a catch-all bin for everything else like veggie scraps and leftovers that is just purely 'compost' for the garden and drains. Or add these to your Bokashi Pro*Biotic* Compost bin.

## **Using your Eco-Pro*biotics***

### **Favored usage include:**

- Hair, oral cavity, body and face wash (Use Eco-Pro*biotics* daily in the morning and evening after cleansing and toning. Sprinkle into palms of hands or use a cotton pad to gently sweep across the face and neck, lightly patting your skin until fully absorbed.)
- Dish washing
- Fruit and vegetable wash
- Evaporative cooler - air freshener
- Insect replant and fertilizer for the garden
- Laundry detergent ... The list goes on.

**If you don't intend to use your Eco-Pro*Biotics*, simply dump it down the toilet in reasonable portions, taking care to ensure no large chunks remain. Use for the garden, or you can dump it in the sewer drain outside in the street.**

## **Other Tips**

- Cut the ingredients into small pieces to let them break down faster.
- Make sure all the organic ingredients are covered by the sugar water.
- Don’t use oily food waste (fish and meat) as ingredients as it creates a bad smell.
- Leave some space at the top of the fermentation vessel to let the mixture ferment.
- Put your fermentation vessel in a well ventilated and cool space away from direct sunlight.
- If small insects are found in the vessel, close the lid tightly and let those insects decompose.
- A white layer on the surface is a sign that fermentation is underway.
- Don’t forget to label your ‘Start Date’!
- The best Eco-Pro*biotics* ferments are 3 months and longer.

Remember: The 3-month period for fermentation starts once your last
organic ingredients are added into the container.

**Frequently Asked Questions**

Q: Should I use only one kind of fruit peels, or many different kinds?

A: Using various ingredients can create complex and stable probiotic ecosystems. Therefore, it is better to use different kinds of fruit peels and vegetable leaf. However! If you’re looking for a product with more specific fragrance profiles, you might want to be more selective.

Q: Will there be bacteria in Eco-Pro*biotics* after fermentation?
Is it safe to be used for personal cleaning?

A: Eco-Pro*biotics* contain acetic acid, pro*biotics*, yeasts, bacteria rich in antioxidants, which help fight bad bacteria on the skin’s surface. Eco Probiotics can reduce allergies caused by chemicals, eliminate harmful microorganisms and enhance cell regeneration. It is important to remember that the acidic value of Eco-Probiotics sits below PH 5.0 if it has been fermented well, so remember to dilute accordingly. Eco-Pro*biotics* is very safe and makes a wonderfully gentle yet powerful wash for wound cleaning. Safe for all family members to use.

Q: How can we make sure that the Eco-Pro*biotics* is well fermented?

A: If the fermentation is successful, the liquid will turn yellowish brown and it will form a white layer on the surface. Otherwise, it will turn black or become moldy. If it turns black, we only have to add brown sugar (same amount) and let it ferment for another month.

**Fermentation: the Key to a lush, happy, life supporting habitat for all.**

#### The Details

##### Natualy Safe and Stable

Metabolites include vitamins, organic acids, amino acids, enzymes, co-enzymes, and bacteriocins. These metabolites are natural pesticides produced by the microbes and can help suppress molds, other microbes, and sometimes even insect pests.

Lactic acid is a strong sterilizing compound that suppresses harmful microorganisms and enhances decomposition of organic matter. LABs promote the fermentation and decomposition of material such as lignin and cellulose, thereby removing undesirable effects of non-decomposed organic matter which can putrefy and release gasses like methane and ammonia that are toxic to plants. 

In soils lactic acid has the ability to suppress disease-inducing microorganisms including the mold Fusarium, which occur in continuous cropping programs. Under normal circumstances, species such as Fusarium weaken crop plants, exposing the plants to diseases and increased pest populations such as pathogenic nematodes. The use of LABs reduces pathogenic nematode populations and controls propagation and the spread of Fusarium, thereby creating a better environment for crop growth. In soils, these probiotic microbes co-exist with other beneficial fungi such as mycorrhizae that are known to attack several pathogenic fungi including fusarium, and also become food themselves to other organisms such as worms.

Eco-Pro*Biotics* can be applied directly to soils as well as sprayed on plants throughout the growing cycle. It is best to apply a probiotic on a regular schedule. For instance, spray once per week throughout the growing season.

#### Lactic acid in the human body

Lactic acid is naturally present in humans, as well as in animals. It is well known that it is formed from glycogen by muscle cells when the oxygen supply is inadequate to support energy production.

L-Lactic acid is a product of the anaerobic (without oxygen) phase of glucose metabolism
(glycolysis) plant and animal cells use for energy. When insufficient oxygen is available for
cells to derive maximum energy from glucose (e.g., bursts of spontaneous activity in muscle
cells requiring more oxygen than is available) excess L-Lactic Acid is produced and diffuses out
of the cells. 

There are many misconceptions about Lactic Acid and Lactate in the body. Lactate is an important fuel used by the muscles during prolonged exercise, lactate produced in one muscle can be oxidized in another muscle. Lactic acid and lactate help delay the onset of fatigue and improve sport performance.

Lactic acid is widely used in food, pharmaceutical cosmetic and other manufacturing sectors. Lactic acid functions as a descaling agent, pH regulator, neutralizer, solvent, humectant, cleaning aid, slow acid-release and antimicrobial agents. 


