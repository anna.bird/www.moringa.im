---
title: Experience: The Vivarium Tour
date: 2018-06-30
---

# *Vivarium Online ~ Salon Style Experiences*
**An interactive safe space for WoMen to explore, learn, reflect; on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces.**

## Designed To Be Fun, Creative, and Resourceful.
**To awaken possibility and creative insight, refine the taste and increase the knowledge of the participants through guided conversation organized around cultures and ideas reflective of the era in which one lives.**

### Hosted Live by ~ Anna
**Artistic Mixed Media Curated Experiences - combining quality live audio with digital and live visual aids that respect bandwidth.** Custom Salon Experiences Available.

#### Experiences Reveal Creative Insight.
**A Self-creative Exploration to insight deep and meaningful change by working with the individuals natural enthusiasm.**

#### *On Location*
**The 'Vivarium Online' will live feed from a portable Eco-studio set on a covered wood patio with zen decor.**

**Many bird, chicken, gecko, frog, chirping insect, ... share this lush, humid subtropical, urban garden surrounding.**

#### Making it Easy, Obvious, Valuable and Normal.
**I'll introduce myself and set the stage by sharing the underlying philosophy and connecting the group, welcoming everyone to the experience.**

**Creating space for a kind of deprogramming from the 'modern story' and an Immersion into a New and Ancient Story.**

**Igniting Your imagination and guiding the experience with constructive, relatable examples from unique lived experiences.**

#### *Come in, please make Yourself comfortable.*
**Prepare some home comforts, relax and immerse Yourself in the experience.**

**Take advantage of the peaceful ambience by connecting with good audio, earbuds or headphone with microphone to participate.**

**With respectful encouragement to engage and question You will influence the direction of the experience.**

#### Curated Salon Style Experiences Listed Below.

-----

# *Fermenting 'Grief' - 'Stress' into Matter

## *Claim Your Inner Resources* by Creating Lasting Healing Connections With 'Mother Earth'.

### *To experience for Yourself, a will to Self Delight, BioPhilia.*
**A Soul separated longing to connect 'Mother Earth' is calling; Your Home.**

#### *Design New Rituals*
***Meaningful ways to respond with honor and dignity, Soil to Soul.***

#### *An Act For The Future*
**Using Your hands to welcome, deepen and widen the conversations, reforming Ones relationship with 'Mother Earth' in Symbiotic Healing.**

#### Journey into the 'Body' of Anima Mundi 'World Soul'.
**Through The Alchemy Of Fermentation One Interacts With The Larger Web Of Life, The Symbiotic Real.**

**Changing the Relationship with Yourself and Your Ancestors by Connecting With Things That Make Living Possible.**

**Healing the Soil to Cultivate Deep Belonging and reclaim Your relationship as a Soul United.**

**Flow of the experience:**
- Welcoming and connecting the group
- Sacred Containers and the Alchemy of Fermentation
- Individual Sharing and Deep Listening, to give and receive
- Natural self healing, the symbiotic healing connection to Mother Earth
- Value of creating healing rituals

#### *Sacred union with Self becomes the eternal relationship with all, effortless, engaging, smiling, evolving dance.*

Online Experience - 90 minutes.
Q&A/Salon Style Open Talk - 20 minutes to close.
Limited to 20 People

--------------

# Creative Eco Living ~ Design A Lifestyle For Change
**Our evolutionary past, is a past in which we evolved with the rest of the biosphere, not separate from it or exempt from its laws.**

## We Are Our Ancestors Wildest Dreams.
**A Unique Opportunity For The Creation Of Something New And Beautiful.**

### Using the symbiotic relationship with 'Mother Earth' as a path of discovery.
**Create A Living Design that relates to 'Earth', Gaia as 'The Live and Dynamic Evolutionary Partner' in the shaping of the set, setting and the process.**

**The relationship between human and environment is highlighted, not human and 'things'.**

**Naturally tap into the abundant emotions of joy, grace, awe, wholeness, passion and compassion that we experience from beauty in nature, bioPhilia.**

#### Values and ethical relations to the natural world, *Reorientation Of Design Through Spirit.*
**It's time to step out from the story of our present situation, and write a New Future. A new story about creating healing spaces and connecting soil to soul.**

**Topics can include:**

- Underlying Philosophy, Designing For Climate Change.
- EnvironMental Health, how it relates to your creative energy.
- Ecological design basics, waste and energy management.
- Designing Living Systems, and microbial gardens, becoming apart of a living system.
- Designing Built Environments, to support healthy body movement.
- The power of medicinal and functional food gardens.
- Understanding Climate Control Built and Physical.

#### Discover how we are designed to connect with things that make living possible.
**Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle.**

Online Experience - 60 minutes.
Q&A/Salon Style Open Talk - 15 minutes to close.
Please request (new)topics prior to attending.

---------------

# Fermented Skin Care Secrets Revealed
**Fermented ingredients are like a supercharged turbo boost for your skincare routine.**

## Learn the way to produce your own high quality therapeutic skincare treatments.
***Naturally; Antiseptic, Anti-Bacterial, Anti-Fungal and Anti-Viral.***

### Nourishing and gentle for the Whole Family.
***Self-care that goes far beyond just the Self, Symbiotic Healing With 'Mother Earth'.***

**Remarkably enough Your Skin, the Water and all that come in contact with that, the Soil, 'Mother Earth' can find natural healing benefits from probiotics with this one easy process.**

#### *Love the Skin Your In*
**A probiotic rich super food for the microbiome of Your Body and Gaia.**

**Engaging “Show & Tell” approach, this class will cover:**

- Overview of a probiotic approach to beauty and wellness.
- The Science behind fermented skin nutrition treatments.
- Introduction of a broad range of natural and seasonal ingredients.
- Live Demo; All Fermentation Steps and details to craft your own personal products at home.
- Insider Information:  tap into over 10 years of knowledge, discussion of multiple uses and application methods.

#### Explore an alternative way to heal and support Your Body, that can heal and support Your Environment.

Online Experience - 60 minutes.
Q&A/Salon Style Open Talk - 15 minutes to close.

-------------

# All You Need To Know ~ DIY Bokashi Composting
**Easy, perfect for the desert, urban settings, communities and schools.**

## *Creating A Culture For Climate Change*
**Learn how Bokashi Is A Solution to some very big problems, Loss of Biodiversity - Waste Disposal - Climate Change.**

### Soul Food Starts With Soil Food
**Experience the soil rejuvenating magic of Probiotics.**

**Bokashi is a centuries-old Japanese culture meaning 'fermented organic matter' and refers to a 2-step method of indoor composting that uses beneficial micro-organisms to quickly ferment many types of food waste including fruit, vegetables, meat, dairy and bread.**

#### Probiotic soul food for the microbiome of the soil.
**Bokashi composting technology is the quantum leap to utilize food and other organic wastes.**

**Engaging “Show & Tell” approach, this class will cover:**

- Overview of a probiotic approach to organic waste management.
- The Science behind Bokashi Composting.
- Learn how Carbon is stored in Soil.
- Live Demo Covering All Steps
- Insider Information:  tap into over 5 years of knowledge.

#### An Act For The Future - Soil Karma
**Offset Your carbon footprint with Your kitchen remnants. A Pro*Biotic* gift of thanks to Gaia, Earth.**

**No 'Garden' No Problem, the Bokashi culture itself is a 'ProBiotic Garden'. Be creative and deposit your offering in the nearest available Earth around you.**

Online Experience - 60 minutes.
Q&A/Salon Style Open Talk - 15 minutes to close.

