---
title: Bokashi Pro*Biotic* Microbiome
date: 2018-08-13
---

# Experience The Soil Reguvinating Magic of Probiotics
**There is no requirment that YOU have a "Garden", the Bokashi itself is a ProBiotic Garden. Be creative and deposit your offering in the nearest Earth around you.**

**Bokashi is a centuries-old Japanese culture meaning 'fermented organic matter' and refers to a 2-step method of indoor composting that uses beneficial micro-organisms to quickly ferment many types of food waste including fruit, vegetables, meat, dairy and bread.**

***The power of this Ancient Culture is not to be missed.***

## Soul Food Starts With Soil Food
**The Gut is to the Brain what the Soil is to the Soul, your Body, all connected through a Microbiome that thrive from ProBiotics.**

**A healthy microbiome, which consists of an overwhelming majority of beneficial probiotic bacteria, is one of the most fundamental aspects of good health, Soil to Soul Food.**

### Bokashi Is Biodiversity
**A healthy microbiome, which consists of an overwhelming majority of beneficial probiotic bacteria, is one of the most fundamental aspects of good health, soil to soul food.**

**What is *Bokashi*? ~ Probiotic soul food for the microbiome of the soil.**

**The microbes in the Bokashi culture mix come from 3 different groups - Lactobacilli, Fungi/yeast, and phototropic bacilli. This combination has been proven to rapidly degrade (ferment) organic waste while suppressing the growth of other potentially dangerous organisms.**

**When Bokashi culture mix is added to organic waste, the microbes immediately begin to grow causing the material to ferment. This system can handle ALL organic waste including cheese, meat, and bones.**

**The standard culture mix is comprised of wheat bran, molasses and the microorganisms.**

#### *The concept of this method is to prevent disease-causing microbes, which usually manifest a putrid odor, and to accelerate degradation process in biodegradable wastes.*

**This natural process has been used among agricultural sectors as fertilizer to plants instead of putting waste into landfills. The method eliminates volatile organic compounds out of its remains such as carbon dioxide and methane which are primary sources of greenhouse gases which contribute to climate change, acidification of oceans and loss of biodiversity.**

**Methane gas released from disposed garbage can trap 21 times more heat than CO2, worsening the global warming condition. The solution... fermentation. No heat or toxic gass is produced in the fermentation process. All while increasing the microbiome of the soil.**

**Fermentation is a kind of detoxification process and transfer of anti-nutrients into nutrients, nutrient enhancement and the creation of micro-nutrients occurs. This process also increases the bioavailability. What all cultured 'foods' have in common is an abundance of lactic acid bacteria, yeasts, and moulds that produce enzymes which break down and alter the original foods.**

**The role of soil microorganisms are many.**
1. break down organic matter
2. recycle nutrients
3. create humus
4. create soil structure
5. fix nitrogen
6. promote plant growth
7. control pests and diseases

### **Step 1: Fermentation** - takes place indoors in your Bokashi bucket.

The system is completely sealed so no mice, rats, cockroaches, or
other pests will be attracted to the fermenting organic waste. You will
not be bothered by fruit flies or any other insects coming to the
waste material.

As you layer your food waste with a starter mix, the microbes present in the starter mix quickly ferment the waste. Once you have filled a bucket you set it aside for 10 - 14 days and start using another bucket.

Two weeks later the initial bucket of waste will be fully fermented and ready for Step 2.

### **Step 2: Decomposition** - is done in the Earth, garden, compost pile, or even in your own soil factory.
Most waste will be indistinguishable from the soil in a week or two, although certain items like bones, egg shells, and corn cobs will take longer to disappear and will act as a slow release probiotic fertilizer. While the waste is decomposing it is still quite acidic so wait 10 days before planting.

The active ingredients in the process are the effective microorganisms – a combination of lactic acid bacteria (also found in kefir whey), photosynthetic bacteria, and yeast - that in effect ‘pickle’ your food waste. These microbes are present in your Bokashi Starter Mix (fermented wheat bran), which is sprinkled over the layers of food waste as you fill your bucket.

The soil microbes very rapidly finish the job converting your fermented organic waste to a rich nutrient soil for your plants. In numbers, the soil microbes may be anywhere from 1,000 to 1,000,000 times more numerous than the fermentation microbes. They will immediately begin to degrade the fermented waste product and they re-establish soil microbial and nutrient content.

Once buried...
Because you buried it in the soil with at least 8 inches of soil over the top, animals will not find the material attractive. Before you cover over the fermented product, mix some soil in with it to help accelerate the second and final conversion to nutrient soil.

Bacteria secrete gums, gels and waxes that help hold the soil together, assist the absorption of water and store nutrients. The moisture content for Bokashi fermented end product is much higher so you are conserving water in the ground.

Nutrients are less prone to leaching and run-off; Because the organic nutrients in the soil after Bokashi fermentation are not as water soluble as are the nutrients derived from composting (by oxidation) they are less prone to leaching away with watering and run-off after rains.

**Building Soil Karma**
In a healthy environment, most of what is digested and transformed into living matter is carbon containing molecules. Carbon is the building block of organic life. Undigested materials include many soluble minerals which are held in the soil by humus and soil organisms. Healthy soil is alive, actively growing and recycling, breathing and digesting literaly symioticly with Us. This is the great recycling system of the Earth. One teaspoon of healthy soil can contain a billion bacteria, a million fungi, and ten thousand amoebae.

**A healthy Bokashi bucket does not create bad odours, greenhouse gas, or heat - uses no power and is completely natural. What you should smell when you open the bucket is a slightly sweet, fermented (sour), perhaps slightly alcoholic odor and it should not be offensive.**

**Bokashi is a great option for food waste of those living in apartments/condos or for those looking to use their food waste as a probiotic nutrient rich soil amendment in their community, garden or plant pots. Give back to the microbiome community.**

## **Benefits of Bokashi**

- No measurable gases are being produced; No methane (or heat) to add to greenhouse gasses
- No bad odors
- Does not desiccate the soil as does compost
- No animals will get into it
- SIMPLE...No turning the heap..once it is buried, you're done
- Can handle ALL organic scraps including bones, meat, and cheese
- 50% faster than ordinary aerobic composting


## **Materials Needed**

- A minimum of two 5-gallon buckets with tight fitting lids is ideal but smaller bins will work just fine
- Absorbent material (peat moss, shredded newspaper, sawdust, dried leaf or grass...)
- Bokashi starter mix (instructions below)
- Food scraps chopped into small pieces

# **Making Bokashi Bran / Starter Mix**

**Molasses : EM/Serum : Warm water(pure) : Absorbent material sawdust (dry leaf...)**

EM means effective micro-organisms. EM consists of mixed cultures of beneficial, naturally occurring micro-organisms such as lactic acid bacteria, yeast, photosynthetic bacteria and actinomycetes.

"EM" can be purchased online or you can make it yourself.

## The Ratio
### 50ml molasses : 50ml "EM"/Serum : 250-300ml water : 5kg absorbent material

Combine all liquids starting with 250ml of warm water.

Pour over your absorbent material, so that when you squeeze it together some of it holds but no liquid comes out. Add more water if you need.

Seal in a bag or use a bucket and put a bag over it and then a lid. You want no air or as little as possible. Seal and ferment for 10 - 14 days.

Sniff test, your "bokashi bran" should smell a little like apple cider vinegar. Its ready to use! For long term storage, lay the fermented "bokashi bran" out on shallow trays and dry in the shade. Once dry you can store it in containers.

#### **Now you have "Bokaski Bran"**

# **Making Your Own Serum - "EM"**

1) Rice Water

To Make Rice Water:

1:2 rice to water ratio, soak the rice for about 15 minutes, stir vigorously then strain and ferment the water in a open air vessel with a cloth cover to allow exposure to the air for 5 - 8 days. Strain

2) Kefir whey

If you don"t have kefir Whey (made from straining kefir milk through cotton cloth to separate the milk solids from the liquid (whey), then you can add your 1 part fermented rice wash to 10 parts whole milk, cover and allow to ferment 10 - 14 days. The ferment will separate into three distinct layers. Use a turkey baster or some such device to retrieve the middle layer.
Strain.

### For the serum combine:

1 part rice water : 10 parts Kefir Whey

Bottle, cap and store in a cool dark place.

**Now you have your bokaski serum (for long storage you can add a little molasses)**

# **Inside the Bokashi Bin**

- Assemble your buckets and start adding your kitchen waste.

- Once you have about 1/2 inch of compacted kitchen waste dust with the bosakhi starter.

- Repeat until the bin is full.

**Once full seal and set aside in a warm dark corner to ferment for 2 weeks.**

**Bury in the Earth or garden, add to your compost bin or use your pots (up to 1/3 by volume).**

## **Bokashi Probiotic Spirit Tea**

The liquid or Bokashi Juice that collects during the process must be removed on a regular basis. Bokashi juice contains nutrients from the food waste and is alive with probiotic micro-organisms so it makes a terrific, free fertiliser!

It is very strong so must be diluted with water at a 100:1 ratio, that's 100 parts water to 1 part bokashi juice, approximately 2 teaspoons of juice for every litre of water.

Pour the concentrated Bokashi probiotic juice or Bokashi Tea directly into kitchen and bathroom drains, toilets and septic systems. It will help prevent algae build-up and control odor. And as a huge bonus, it contributes to cleaning up our waterways as the good bacteria compete with the bad bacteria! You can also pour this liquid directly into your compost bin.

## **Fermentation: the Key to a lush, happy, life supporting habitat for all.**
