+++
title= "Vivarium - Entire Residence Private Stay"
date= 2018-12-28
tags= eco, sustainability, urban, nomadic, lifestyles
+++

# *Private Residence Stay*

## Vivarium ~ place of life

### *Creative Living Design*

### Support our work and get behind-the-scenes experience with our Private Residence Stay.

### Boutique Condo - Full access to the 1100sq.ft Vivarium; 3 bedroom, 2 bathroom, living area, patio, laundry, kitchen. The space all to yourself(s).- accommodating 5 adults - top 3rd floor north facing unit.

#### An interactive space for people to Explore, Learn, Reflect, ... on how we Interact, Impact and Live in our personal environments; homes, work, travel, ..., spaces.

### *Knowledge to design a more desirable, fulfilling, life supporting; Ecologically modern and Realistic lifestyle.*

**It's time to step out from the story of our present situation, and write a New Future. A new story about healing and conecting Soil to Soul. The Vivarium Microbiome offers space for a kind of deprogramming from the 'modern story' and an immersion into a new and ancient story. Discover the importance of a healthy 'microbiome' in our bodies and the soil.**

**Vivarium Ideal Candidate:**

- Has an open, positive, and willing attitude
- Wishes to dedicate time to a focused period of creative Eco-learning
- Is emotionally and psychologically stable
- Is self-motivated, interested in learning and personal growth

#### **The Details**

**Included in your stay (by request):**

- Full working knowledge of The Vivarium and all Pro*Biotic* Experiences. The time you dedicate to learning is up to you, we work together at your pace. 

- Personal, live on-line and in-house private instruction and support; befor, durring and after your stay.

**The heart of this 'Private Resident Stay' is to work with the individuals natural enthusiasm, a self-creative exploration. To support it so that it becomes a way of living in a deep and meaningful manner, lifelong. This is very much self-initiated.**

## *Body and Soil* - Pro*Biotic* Experience Options

***Ancient technologies for a modern world; restoring microbe biodiversity.***

#### Pro*Biotic* Medicinal Kitchen:

- [**Kefir Milk**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-Kefir-Milk.md)
- **Kefir Cheese**
- [**Lacto Fermentation**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-lactoFerments.md)

#### Pro*Biotic* Medicinal Bath & Garden: Skin, Soil and Water Rehabilitation

- [**EcoClean Probiotic Body Wash and Home Cleaning and Healing**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-EcoClean.md)
- [**Probiotic Bokashi Composting - Soil Microbiome Garden**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-Bokashi.md)

**You can leave fully understading how the Vivarium functions and able to share the knowledge with others. Please know we are open to colaboration and encourage individuals to share what they feel would be of benifit to this work.**

**This immersive stay has a lot to offer those looking to make their lifestyles more Holistic, if your interested in ecological / holistic living, minimalism, zero-waste, environmental studies or simply want to experience a Pro*Biotic* Lifestyle on for size... this stay may be a great fit, offering a broad range of applications.**

**Rates subject to change, please enquire.**
- one week 280USD - extra days 40USD 
- two weeks 540USD - extra days 38USD 
- monthly 820USD - extra days 29USD

**Limited availability, plan in advance. Shorter stays will be considered.**

# The Residence - Vivarium Private Stay

Boutique condo located on a quiet one-way road in a safe low density neighborhood with large trees and properties with plenty of green space. The private 15 unit residence is set well back from the street with a large front garden perfect for a picnic or morning yoga. The gated property provides dusk to dawn manned security.

### *The Space*

All bedrooms have several large opening garden windows, ceiling fan, mosquito net to cover the bed, and healthy low voltage LED lighting. The decor is natural bamboo, wood, coconut and rattan. Handmade textiles of organic cotton, silk and hemp decorate the walls.

The Vivarium is designed to encourage healthy body movement, to relax and work. 

Living room is open and spacious with beautiful white marble floors. Wi-Fi is centrally located in this room. Enjoy a tea on the patio and discover a forest garden of edible, nutrient dense, medicinal plants. Experience the company of visiting birds, they are always around, singing, exploring and foraging in the garden.

Kitchen is spacous with everything you need to cook fast, simple modest meals. Please freely forage from the gardens of the Vivarium. Laundry area is just outside the kitchen.

The unique design of the boutique residence offers each condo _three outside walls with large opening garden windows. Due to this design we enjoy a wonderful _open feel and fantastic cross breeze from our north facing unit. We consciously chose to enjoy the smell, sound and feel of an A/C free environment. Intentional privacy screening allows guests to open their door and take advantage of the natural flow of fresh air. Additionally we offer an evaporative cooler in the common area, essential oils can be added, this helps to create a fresh and pleasant micro climate.

Corner master bedroom, features a 4pc ensuite, queen size bed, custom made Polynesian style wood platform and dedicated evaporative cooler. For the addition of essential oils please inquire. Spacious, even with luggage and two of you in the room one can enjoy yoga. Cotton linens and towels provided.

Private attached bathroom has a large vanity, mirror, sink, toilet, small window and full size bathtub with shower. This bathroom does not have hot water, for a hot shower access the shared bathroom just outside your room.

Double bedroom features a large corner window garden, natural wood parquet floor and two coconut tatami mats, each on there own custom made Polynesian style wood platform. 

Single bedroom features a large corner window garden, natural wood parquet floor and single coconut tatami mat on a custom made Polynesian style wood platform. Cotton linens and towels are provided.

Enjoy a cooler more supported nights sleep. Coconut tatami mats offer spinal support, are naturally breathable and recommended for those that suffer allergies. Cotton linens and towels are provided.

Shared bathroom (located just outside the doudle and single rooms) provides a mirror above the sink, toilet and standing shower with hot water.

#### Other things to note

Kitchen must be kept clean and orderly at all times, please do not leave any food, food scraps or dirty dishes as this attracts insects. 

Upon departure please ensure that the kitchen is clean and no aditional laundry is left.

Drinking water can be arranged for delivery.
The Vivarium has one locked utility room off the laundry.

We are quite strict about the house rules, this helps provide a clean and quiet environment to stay.

**View the Vivarium resdence with this link:**
[**Vivarium - Private 3 bedroom 2 bathroom Condo Residence**](https://airbnb.com/h/private-safe-condo-creative-ecoliving-penang-georgetown)

#### ***View all Vivarium listings and read reviews***
[**The Vivarium - place of life**](https://www.airbnb.com/users/show/3774308)
