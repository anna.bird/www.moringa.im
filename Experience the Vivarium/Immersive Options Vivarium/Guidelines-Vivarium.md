---
title: Vivarium Guidelines
date: 2018-08-20
---

# *Vivarium Microbiome Living ~ place of life*

## Creative Eco-living

### Welcome to your interactive stay!

#### The most important thing you must actively commit to durring your stay is **Watering The Gardens Daily.

#### We ask that guest honor this experience by fully participating during their stay and refrain from using personal care and body products that contain chemicals, artificial scents and other harmful agents.

**We are a shoe free environment, please leave outdoor shoes at the entrance to the vivarium.**

**Please shower before you lay in the bed, and please do not lay in the bed with street clothes on.** 

**Leave windows open as much as possible for nice air flow. Open the door of your room to enjoy the flow.**

**Evaporative coolers are for your use, please make sure they have water. Mop up any spills.**

**Private Stay and Co-hosts must stay clean and tidy. Please do not leave dirty dishes out as this attracts insects, you will need to be clean in the kitchen. Please do not leave a dirty kitchen or extra laundry on departure date.**

**Other things to note**

No deliveries after 11pm
Quiet hours from 11pm to 7am
Drinking water delivery can be arranged.
The Vivarium has one locked utility room off the laundry.

This space is shared, we are quite strict about our house rules. This helps provide a clean and quiet environment to stay. If you have any questions please ask prior to making your reservation.


