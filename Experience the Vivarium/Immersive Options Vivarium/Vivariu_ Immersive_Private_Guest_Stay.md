+++
title= "Vivarium Immersive ~ Private Guest Stay"
date= 2018-12-28
type = Listing
tags= [ eco, sustainability, urban, nomadic, lifestyles ]
+++

# *Private Guest Stay*

## Vivarium ~ place of life

### *Creative Living Design*

### Support our work and get behind-the-scenes experience.

### Boutique Condo - Vivarium; 3 bedroom, 2 bathroom, living area, patio, with laundry service. Top - 3rd floor north facing unit.

### An interactive space for people to Explore, Learn, Reflect, ... on how we Interact, Impact and Live in our personal environments; homes, work, travel, ..., spaces.

**It's time to step out from the story of our present situation, and write a New Future. A new story about healing and conecting Soil to Soul. The Vivarium Microbiome offers space for a kind of deprogramming from the 'modern story' and an immersion into a new and ancient story. Discover the importance of a healthy 'microbiome' in our bodies and the soil.**

## *Knowledge to design a more desirable, fulfilling, life supporting; Ecologically modern and realistic lifestyle.*

**Vivarium Ideal Candidate:**

- Has an open, positive, and willing attitude
- Wishes to dedicate time to a focused period of creative Eco-learning
- Is comfortable living in a collective situation
- Is emotionally and psychologically stable
- Is self-motivated, interested in learning and personal growth

**The Details**

Included in your stay (by request):

- Full working knowledge Pro*Biotic* Experiences listed below. The time you dedicate to learning is up to you, we work together at your pace.

## *Body and Soil* - Vivarium Experience Options
**Personal In House / Live Online Instruction and Support**

**Ancient technologies for a modern world; restoring microbe biodiversity.**

#### Pro*Biotic* Medicinal Bath & Garden: Skin, Soil and Water Rehabilitation

- [**EcoClean Probiotic Body Wash and Home Cleaning and Healing**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-EcoClean.md)
- [**Probiotic Bokashi Composting - Soil Microbiome Garden**](https://gitlab.com/anna.bird/www.moringa.im/blob/master/Experience%20the%20Vivarium/Programs/Program%20Options/program-Bokashi.md)

**You can leave fully understading how the Vivarium functions and able to share the knowledge with others. Please know we are open to colaboration and encourage individuals to share what they feel would be of benifit to this work.**

**This experience has a lot to offer those looking to make their lifestyles more Holistic, if your interested in ecological / holistic living, minimalism, zero-waste, environmental studies or simply want to experience a Pro*Biotic* Lifestyle on for size... this program may be a great fit, offering a broad range of applications.**

**Rates - Nightly (two night minimum)**
- Single 14
- Double 16
- Queen ensuite 20

**Rates subject to change, please enquire. Limited availability, plan in advance.**

**Private Queen Room view with this link:**
[**Queen Bed w/ Private Bath**](https://www.airbnb.com/rooms/15590733?s=51)

Corner master bedroom, features a 4pc ensuite, queen size bed, custom made Polynesian style wood platform and dedicated evaporative cooler. For the addition of essential oils please inquire. Spacious, even with luggage and two of you in the room one can enjoy yoga. Cotton linens and towels provided.

Private attached bathroom has a large vanity, mirror, sink, toilet, small window and full size bathtub with shower. This bathroom does not have hot water, for a hot shower access the shared bathroom just outside your room.

**Private Double Room view with this link:**
[**Double Bed Coconut Fiber Tatami Mats - Shared Bath**](https://www.airbnb.com/rooms/15553849?s=51)

Double bedroom features a large corner window garden, natural wood parquet floor and two coconut tatami mats, each on there own custom made Polynesian style wood platform. Coconut tatami mats offer spinal support, are naturally breathable and recommended for those that suffer allergies. Cotton linens and towels are provided.

Shared bathroom, located just outside this room, provides a mirror above the sink, toilet and standing shower with hot water.

**Private Single Room view with this link:**
[**Single Bed Coconut Fiber Tatami Mat - Shared Bath**](https://airbnb.com/h/private-room-penang-creative-eco-living-garden-vivarium)

Enjoy a cooler more supported nights sleep.
Corner windo room features a single coconut tatami mat on a custom made Polynesian style wood platform. Coconut tatami mats offer spinal support, are naturally breathable and recommended for those that suffer allergies. Cotton linens and towels are provided.

Shared bathroom, located just outside this room, provides a mirror above the sink, toilet and standing shower with hot water.

#### ***View Vivarium listings and read reviews***
[**The Vivarium - place of life**](https://www.airbnb.com/users/show/3774308)


