---
title: Contact
description: Join us on the Matrix Network.
type: page
menu:
  main: {}

---


We communicate exclusively on the federated Matrix network.
### Join our landing matrix room: 
## [#organic:matrix.org](https://riot.im/app/#/room/#organic:matrix.org)

You can use the [Riot](https://about.riot.im) app to create a free account.
